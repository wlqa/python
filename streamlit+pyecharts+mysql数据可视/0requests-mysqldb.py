import re
import requests,pymysql
from bs4 import BeautifulSoup
from lxml import etree
conn=pymysql.connect(host='127.0.0.1',port=3306,user='root',passwd='root',db='lvnengdb',charset='utf8')
cursor=conn.cursor()   

sql = """create table 绿色能源卖方总表(
		卖家公司 varchar(150) not null,
		核发张数 varchar(30) not null,
		日期 varchar(30) not null
        )"""
sql2 = """create table 绿色能源买方总表(
		买家 varchar(150) not null,
		认购张数 varchar(30) not null,
		日期 varchar(30) not null
        )"""
sql3 = """create table 绿电产品详情(
		项目名称 varchar(150) not null,
		绿电生产时间 varchar(30) ,
		`价格/元` float ,
		公司名称 varchar(100) ,
		购买人数 varchar(30) ,
		地址 varchar(30) 
        )"""     
sql4 = """create table 绿电公司详情(
		公司名称 varchar(150) not null,
		项目名称 varchar(100) ,
		项目类型 varchar(100) ,
		项目规模 varchar(30) ,
		`单价/元`  int ,
		项目批次 varchar(30) ,
		所属集团 varchar(50) ,
		核证时间 varchar(30) ,
		绿证编号 varchar(60) ,
		库存	 int,
		已售 int,
		并网时间 varchar(30) ,
		项目地址 varchar(30) ,
		联系卖家 varchar(30) 
        )"""  

sql5 = """create table 绿电公司交易记录(
		公司名称 varchar(150) ,
		项目类型 varchar(100) ,
		金额 float ,
		数量 int ,
		日期 varchar(30) 
        )"""                  
cursor.execute(sql)
cursor.execute(sql2)
cursor.execute(sql3)
cursor.execute(sql4)
cursor.execute(sql5)
conn.commit()
cursor.close()
conn.close()
a = []
header = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; rv:46.0) Gecko/20100101 Firefox/46.0'}
res = requests.get('http://www.greenenergy.org.cn/',headers=header)
res.encoding='utf-8'
soup = BeautifulSoup(res.text, 'html.parser')
#############################################===========绿电买卖方详情=========######################################################################
for li_tag in  soup.find_all('ul',class_="main_list_one fl"):		#查找ul标签下的class_="main_list_one fl" 内容
	for span_tag in li_tag.find_all('li'):   						#查找ul标签下的li标签
		maijia = span_tag.find('span').text 						#获取li标签下的第一个span标签的文本内容
		hefa=span_tag.find_all('span')[1].text 						#获取li标签下的第二个span标签的文本内容
		hefa = re.findall(r'\d', hefa)
		hefa = ''.join(hefa)
		print(hefa)
		time = span_tag.find_all('span')[2].text 					#获取li标签下的第三个span标签的文本内容
		sql = 'insert into 绿色能源卖方总表(卖家公司,核发张数,日期) values ("%s","%s","%s")' % (maijia,hefa,time)
		cursor.execute(sql)

for li_tag in  soup.find_all('ul',class_="main_list_two fl"):
	for span_tag in li_tag.find_all('li'):
		maijia = span_tag.find('span').text

		rengou=span_tag.find_all('span')[1].text
		rengou =  re.findall(r'\d', rengou)
		rengou = ''.join(rengou)
		print(rengou)
		time = span_tag.find_all('span')[2].text
		sql = 'insert into 绿色能源买方总表(买家,认购张数,日期) values ("%s","%s","%s")' % (maijia,rengou,time)
		cursor.execute(sql)
#############################################===========绿电产品列表详情=========######################################################################		

shengfen = []
page = 1
while page < 156: # 共155页，这里是手工指定的
	priceurl = requests.get('''http://www.greenenergy.org.cn/gctrade/shop/product/listproductCategoryId.jhtml?projectType=&projtype=&province=&city=&companyId=&
		companyName=&startPrice=&endPrice=&keyword=&pageSize=5&searchProperty=&orderProperty=&orderDirection=&pageNumber={}'''.format(page),headers=header)
	priceurl.encoding='utf-8'
	price_soup = BeautifulSoup(priceurl.text, 'html.parser')
	#print(price_soup)
	for li_tag in  price_soup.find_all('ul',style="margin-top:0px;padding:0px"):		#提取全部绿电卖家公司，ul标签下的内容
		#print(li_tag)
		for div_tag in li_tag.find_all('li'):	
			#print(div_tag)
			sleep = 5					
			project = div_tag.find_all('p')[0].text.strip()					#提取li标签下的第一个p标签  项目名称 
			print(project)
			project = project.strip()	#去掉项目名称多余空格
			print(project)
			time = div_tag.find_all('p')[1].text					#绿电生产时间
			time = ''.join(re.findall(r'[^绿电生产时间:]', time)).strip() #去除多余数字和空格
			print(time)
			price = div_tag.find_all('span')[2].text.strip()				#提取li标签下的第3个标签  单价
			print(price)
			company = div_tag.find_all('span')[3].text.strip()				#公司名称
			print(company)
			buy = div_tag.find_all('span')[4].text					#购买人数
			buy = ''.join(re.findall(r'[^人已经购买]', buy)).strip()
			print(buy)
			site = div_tag.find_all('span')[5].text.strip()				#项目地址
			print(site)
# #############################################===========绿电公司详情=========######################################################################
			CompanyD = div_tag.find_all('span')[6].text		#确定有立即购买的选项
			# #print(CompanyD)
			if CompanyD.__contains__('立即购买'): 
				CompanyD1 = div_tag.find_all('a')				#提取ul标签下的li标签下的第六个span标签的链接
				for link in CompanyD1:
					CompanyDlink = link['href']
					CompanyDlink = 'http://www.greenenergy.org.cn{}'.format(CompanyDlink)	#提取a标签下完整的域名
					#print(CompanyDlink)
					CompanyDlink =requests.get(CompanyDlink,headers=header) 				#爬取点击购买下的链接
					CompanyDlink.encoding='utf-8'
					CompanyDlink = etree.HTML(CompanyDlink.text)
					gsmc = CompanyDlink.xpath('//*[@id="shop_xxnr1"]/ul/li[1]/span[2]/text()')	
					gsmc = gsmc[0].strip()																#公司名称
					print(gsmc)
					xmmc = CompanyDlink.xpath('//div[3]/div[1]/div[2]//ul[1]/li[1]/text()')[0]	#项目名称
					xmmc = ''.join(re.findall(r'[^项目名称：]', xmmc))
					dj = CompanyDlink.xpath('//*[@id="dj"]/text()')[0]							#单价
					xmpc = CompanyDlink.xpath('//div[3]/div[1]/div[2]//ul[1]/li[3]/text()')[0]	#项目批次
					xmpc = ''.join(re.findall(r'[^项目批次：]', xmpc))
					ssjt = CompanyDlink.xpath('//div[3]/div[1]//ul[1]/li[4]/text()')[0]			#所属集团
					ssjt = ''.join(re.findall(r'[^所属集团：]', ssjt))
					hzsj = CompanyDlink.xpath('//div[3]/div[1]//ul[1]/li[5]/text()')[0]			#核证时间
					hzsj = ''.join(re.findall(r'[^核证时间：]', hzsj))
					lzbh = CompanyDlink.xpath('//div[3]//ul[1]/li[6]/text()')[0]				#绿证编号
					lzbh = ''.join(re.findall(r'[^绿证编号：]', lzbh))
					kc = CompanyDlink.xpath('//div[3]//ul[1]/li[8]/text()')[0]					#库存
					kc = ''.join(re.findall(r'[\d]', kc))
					ys = CompanyDlink.xpath('//div[3]//ul[1]/li[9]/text()')[0]					#已售
					ys = ''.join(re.findall(r'[\d]', ys))
					xmlx = CompanyDlink.xpath('//*[@id="shop_xxnr1"]/ul/li[2]/text()')
					xmlx = xmlx[0]																#项目类型
					bwsj = CompanyDlink.xpath('//*[@id="shop_xxnr1"]/ul/li[3]/text()')
					bwsj = bwsj[0]																#并网时间
					xmgm = CompanyDlink.xpath('//*[@id="shop_xxnr1"]/ul/li[4]/text()')
					xmgm = xmgm[0]																#项目规模
					xmdz = CompanyDlink.xpath('//*[@id="shop_xxnr1"]/ul/li[5]/span[2]/text()')
					xmdz = xmdz[0]																#项目地址
					print(xmdz)

					try:
						lxdh = CompanyDlink.xpath('//*[@id="lxmj"]/text()')
						lxdh = lxdh[0]															#联系卖家
					except:
						lxdh = "无电话"
					print(xmmc)
					a.append(xmmc)
					
# #############################################===========绿电交易记录=========######################################################################
					try:
						xmmc2 = CompanyDlink.xpath('//*[@id="shop_xxnr3"]/table//tr[2]/td[1]/a/text()')  #获取点击购买页面下的公司交易记录
						xmmc2 = xmmc2[0]			#项目名称
						jh = CompanyDlink.xpath('//*[@id="shop_xxnr3"]/table//tr[2]/td[2]/text()')
						jh = jh[0] 		#金额
						jh = re.findall(r'[^￥]', jh)
						jh = ''.join(jh)#金额
						sl = CompanyDlink.xpath('//*[@id="shop_xxnr3"]/table//tr[2]/td[3]/text()')
						sl = sl[0]			#交易数量
						rq = CompanyDlink.xpath('//*[@id="shop_xxnr3"]/table//tr[2]/td[4]/text()')
						rq = rq[0]			#交易日期
						
					except:
						xmmc2 = '暂无销售'
						jh = 0
						sl = 0
						rq = None
# #############################################===========绿电交易记录=========######################################################################
			
			sql = 'insert into 绿电产品详情(项目名称,绿电生产时间,`价格/元`,公司名称,购买人数,地址) values ("%s","%s","%s","%s","%s","%s")' % (project,time,price,company,buy,site)				
			sql2 = 'insert into 绿电公司详情(公司名称,项目名称,项目类型,项目规模,`单价/元`,项目批次,所属集团,核证时间,绿证编号,库存,已售,并网时间,项目地址,联系卖家) values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' % (gsmc,xmmc,xmlx,xmgm,dj,xmpc,ssjt,hzsj,lzbh,kc,ys,bwsj,xmdz,lxdh)
			sql3 = 'insert into 绿电公司交易记录(公司名称,项目类型,金额,数量,日期) values ("%s","%s","%s","%s","%s")' % (gsmc,xmmc,jh,sl,rq)      
			cursor.execute(sql)
			cursor.execute(sql2)
			cursor.execute(sql3)
	print("第",page,"页")
	page = page + 1
conn.commit()
cursor.close()
conn.close()


